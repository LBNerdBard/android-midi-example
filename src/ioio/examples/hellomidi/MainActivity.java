package ioio.examples.hellomidi;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ArrayBlockingQueue;


import ioio.examples.hellomidi.R;
import ioio.javax.sound.midi.InvalidMidiDataException;
import ioio.javax.sound.midi.MidiMessage;
import ioio.javax.sound.midi.ShortMessage;
import ioio.lib.api.Uart;
import ioio.lib.api.DigitalOutput.Spec;
import ioio.lib.api.DigitalOutput.Spec.Mode;
import ioio.lib.api.Uart.Parity;
import ioio.lib.api.Uart.StopBits;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.AbstractIOIOActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * This is the main activity of the HelloIOIO example application.
 * 
 * It displays a toggle button on the screen, which enables control of the
 * on-board LED. This example shows a very simple usage of the IOIO, by using
 * the {@link AbstractIOIOActivity} class. For a more advanced use case, see the
 * HelloIOIOPower example.
 */
public class MainActivity extends AbstractIOIOActivity{
	private Button button_;
	private SeekBar mix_;
	
	/** a queue of midi bytes**/
	private final ArrayBlockingQueue<MidiMessage> out_queue = new ArrayBlockingQueue<MidiMessage>(OUT_QUEUE_SIZE);
	private final static int OUT_QUEUE_SIZE = 100;
	
	private final static String DEBUG_TAG = "IOIOMIDI";
	


	/**
	 * Called when the activity is first created. Here we normally initialize
	 * our GUI.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	
		button_ = (Button) findViewById(R.id.button);
		button_.setOnTouchListener(new OnTouchListener(){

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				switch(arg1.getAction()){
				case MotionEvent.ACTION_DOWN:
					playNoteC();
					return true;
				case MotionEvent.ACTION_UP:
					stopNoteC();
					return true;
				}
				return false;
			}
			
		});
		
		
		mix_ = (SeekBar)findViewById(R.id.seekbar_mix);
		mix_.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				ShortMessage msg = new ShortMessage();
				try {
					msg.setMessage(ShortMessage.CONTROL_CHANGE, 33, arg1);
					out_queue.add(msg);
				} catch (InvalidMidiDataException e) {
					Log.e(DEBUG_TAG,"InvalidMidiDataException caught");
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {}
			
		});
		
	}
	
	/** plays a 'C' MIDI note 
	 * @throws InvalidMidiDataException **/
	private void playNoteC(){
		Log.i(DEBUG_TAG,"Playing note C");
		ShortMessage msg = new ShortMessage();
		try {
			msg.setMessage(ShortMessage.NOTE_ON, 60, 60);
			out_queue.add(msg);
		} catch (InvalidMidiDataException e) {
			Log.e(DEBUG_TAG,"InvalidMidiDataException caught");
		}
		
	}
	
	/** stops a 'C' MIDI note 
	 * @throws InvalidMidiDataException **/
	private void stopNoteC(){
		Log.i(DEBUG_TAG,"Stopping note C");
		ShortMessage msg = new ShortMessage();
		try {
			msg.setMessage(ShortMessage.NOTE_ON, 60, 0);
			out_queue.add(msg);
		} catch (InvalidMidiDataException e) {
			Log.e(DEBUG_TAG,"InvalidMidiDataException caught");
		}
	}

	/**
	 * This is the thread on which all the IOIO activity happens. It will be run
	 * every time the application is resumed and aborted when it is paused. The
	 * method setup() will be called right after a connection with the IOIO has
	 * been established (which might happen several times!). Then, loop() will
	 * be called repetitively until the IOIO gets disconnected.
	 */
	class IOIOThread extends AbstractIOIOActivity.IOIOThread {
		
		private Uart midi_out_;
		/** the output stream used to send the midi bytes **/
		private OutputStream out;
		/**
		 * Called every time a connection with IOIO has been established.
		 * Typically used to open pins.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#setup()
		 */
		@Override
		protected void setup() throws ConnectionLostException {
			midi_out_ = ioio_.openUart(null,new Spec(7,Mode.OPEN_DRAIN), 31250,Parity.NONE,StopBits.ONE);
			out = midi_out_.getOutputStream();
		}

		/**
		 * Called repetitively while the IOIO is connected.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#loop()
		 */
		@Override
		protected void loop() throws ConnectionLostException {	
			if (!out_queue.isEmpty()){
				try {
					out.write(out_queue.poll().getMessage());
				} catch (IOException e) {
					Log.e(DEBUG_TAG,"IOIOException caught");
				}	
			}	
		}
	}

	/**
	 * A method to create our IOIO thread.
	 * 
	 * @see ioio.lib.util.AbstractIOIOActivity#createIOIOThread()
	 */
	@Override
	protected AbstractIOIOActivity.IOIOThread createIOIOThread() {
		return new IOIOThread();
	}

	
}